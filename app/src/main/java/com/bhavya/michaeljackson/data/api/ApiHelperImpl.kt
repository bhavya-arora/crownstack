package com.bhavya.michaeljackson.data.api

import com.bhavya.michaeljackson.data.model.ApiSong

class ApiHelperImpl(private val apiService: ApiService): ApiHelper {

    override suspend fun getSongs(term: String): List<ApiSong>  {
        var serviceResponse = apiService.getSongs(term)
        return serviceResponse.results as List<ApiSong>
    }


}