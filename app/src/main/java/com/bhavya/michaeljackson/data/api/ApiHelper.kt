package com.bhavya.michaeljackson.data.api

import com.bhavya.michaeljackson.data.model.ApiSong
import com.bhavya.michaeljackson.data.model.ApiSongResult

interface ApiHelper {

    suspend fun getSongs(term: String): List<ApiSong>
}