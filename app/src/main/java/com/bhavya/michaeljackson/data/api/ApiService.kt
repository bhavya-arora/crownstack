package com.bhavya.michaeljackson.data.api

import com.bhavya.michaeljackson.data.model.ApiSong
import com.bhavya.michaeljackson.data.model.ApiSongResult
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("search")
    suspend fun getSongs(
        @Query("term") term: String
    ): ApiSongResult
}