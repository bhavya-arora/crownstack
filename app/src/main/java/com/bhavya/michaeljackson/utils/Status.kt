package com.bhavya.michaeljackson.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}