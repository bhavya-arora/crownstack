package com.bhavya.michaeljackson.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bhavya.michaeljackson.data.api.ApiHelper
import com.bhavya.michaeljackson.ui.song.SongViewModel

class ViewModelFactory(private val apiHelper: ApiHelper) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SongViewModel::class.java)) {
            return SongViewModel(apiHelper) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }

}