package com.bhavya.michaeljackson.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.INFO
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bhavya.michaeljackson.R
import com.bhavya.michaeljackson.data.api.ApiHelper
import com.bhavya.michaeljackson.data.api.ApiHelperImpl
import com.bhavya.michaeljackson.data.api.RetrofitBuilder
import com.bhavya.michaeljackson.data.model.ApiSong
import com.bhavya.michaeljackson.ui.song.SongListAdapter
import com.bhavya.michaeljackson.ui.song.SongViewModel
import com.bhavya.michaeljackson.ui.songdetail.SongDetailActivity
import com.bhavya.michaeljackson.utils.Status
import com.bhavya.michaeljackson.utils.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SongListAdapter.SongItemSelected{

    private lateinit var viewModel: SongViewModel
    private lateinit var adapter: SongListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupUi()
        setupViewModel()
        setupObservers()
    }

    private fun setupUi() {
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = SongListAdapter(
            arrayListOf(),
            this
        )

        recyclerView.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.getFetchedSongs().observe(this, Observer {
            when(it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    it.data?.let {
                        songs ->
                            render(songs)
                    }
                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService)
            )
        ).get(SongViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_song, menu)

        val searchItem = menu!!.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if(query != null) {
                    recyclerView.scrollToPosition(0)
                    viewModel.querySongs(query)
                    searchView.clearFocus()
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

        })
        return true
    }

    //Utils
    private fun render(songs: List<ApiSong>) {
        adapter.addData(songs)
        adapter.notifyDataSetChanged()
    }

    override fun itemSelected(item: ApiSong) {
        SongDetailActivity.startActivity(this, item)
    }
}