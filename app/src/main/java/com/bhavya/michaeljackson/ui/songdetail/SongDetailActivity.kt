package com.bhavya.michaeljackson.ui.songdetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bhavya.michaeljackson.R
import com.bhavya.michaeljackson.data.model.ApiSong
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_song_detail.*
import kotlinx.android.synthetic.main.item_song.view.*

class SongDetailActivity : AppCompatActivity() {
    companion object {
        private const val SONGDETAIL = "song_detail"
        fun startActivity(context: Context, songItem: ApiSong) {
            val myIntent = Intent(context, SongDetailActivity::class.java)
            myIntent.putExtra(SONGDETAIL, songItem) //Optional parameters

            context.startActivity(myIntent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_detail)

        setupToolbar()
        setupData()
    }

    private fun setupData() {
        val songItem = intent.getParcelableExtra<ApiSong>(SONGDETAIL)
        Glide.with(iv_artist.context)
            .load(songItem.artworkUrl100)
            .into(iv_artist)

        tv_title.setText(songItem.trackName)
    }

    private fun setupToolbar(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    // don't forget click listener for back button
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}