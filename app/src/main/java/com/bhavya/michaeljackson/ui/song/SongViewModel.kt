package com.bhavya.michaeljackson.ui.song

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bhavya.michaeljackson.data.api.ApiHelper
import com.bhavya.michaeljackson.data.model.ApiSong
import com.bhavya.michaeljackson.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception

class SongViewModel(
    private val apiHelper: ApiHelper
): ViewModel() {

    private val songs = MutableLiveData<Resource<List<ApiSong>>>()

    init {
        getSongs(defaultQuery)
    }

    private fun getSongs(queryString: String) {
        viewModelScope.launch {
            songs.postValue(Resource.loading(null))

            try {
                val songsFetched = apiHelper.getSongs(queryString)
                songs.postValue(Resource.success(songsFetched))

            } catch (e: Exception) {
                songs.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    fun getFetchedSongs(): LiveData<Resource<List<ApiSong>>> {
        return songs
    }

    fun querySongs(queryString: String) {
        val listQuery = queryString.split(" ")
        val finalQueryString = listQuery.joinToString(
            prefix = "",
            postfix = "",
            separator = "+"
        )

        Log.d("MainActivityLog", "Final Query ${finalQueryString}")

        getSongs(finalQueryString)
    }

    companion object {
        private const val defaultQuery = "Michael+jackson"
    }

}