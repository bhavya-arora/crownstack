package com.bhavya.michaeljackson.ui.song

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bhavya.michaeljackson.R
import com.bhavya.michaeljackson.data.model.ApiSong
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_song.view.*

class SongListAdapter(private val arrSongList: ArrayList<ApiSong>, private val songItemSelected: SongItemSelected):
    RecyclerView.Adapter<SongListAdapter.MyViewHolder>(){

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            init {
                itemView.setOnClickListener {
                    songItemSelected.itemSelected(arrSongList[adapterPosition])
                }
            }

            val songDesc: TextView = itemView.tv_desc
            val artwork: ImageView = itemView.iv_artwork
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = arrSongList.get(position)

        holder.songDesc.setText(currentItem.trackName)
        Glide.with(holder.itemView.iv_artwork.context)
            .load(currentItem.artworkUrl100)
            .into(holder.itemView.iv_artwork)
    }

    override fun getItemCount(): Int {
        return arrSongList.size
    }

    fun addData(list: List<ApiSong>) {
        arrSongList.clear()
        arrSongList.addAll(list)
    }

    interface SongItemSelected {
        fun itemSelected(item: ApiSong)
    }
}